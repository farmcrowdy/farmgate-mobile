
import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AnimatedSplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<AnimatedSplashScreen>
    with SingleTickerProviderStateMixin {
  var _visible = true;
  var token;

  AnimationController animationController;
  Animation<double> animation;
  String home='/WelcomePage';
  String login='/WelcomePage';





  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }
  void navigationPage() async{
    //SharedPreferences prefs = await SharedPreferences.getInstance();

    //token = prefs.getString('token');

    print(token);

    token == null?
    Navigator.of(context).pushReplacementNamed(login):
   Navigator.of(context).pushReplacementNamed(home);
  }
  @override
  void initState() {

    super.initState();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 2));
    animation =
    new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    startTime();
  }
 //
  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery
        .of(context)
        .size
        .height;

    final deviceWidth = MediaQuery
        .of(context)
        .size
        .width;
    return   Stack( // <-- STACK AS THE SCAFFOLD PARENT
      children: [
      Container(
        color: Colors.white,
      height: deviceHeight,
      width:deviceWidth ,
      child:
      Image.asset('images/background.png',fit: BoxFit.fill,),
    ),
        // TODO: implement build
        Scaffold(
            backgroundColor: Colors.transparent,
            body:

           Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child:
              Container(
                //padding: EdgeInsets.all(50),
                height: 216,
                width: 233,
                child:
              new Image.asset(
                'images/logo.png',
              ),
              ),
              ),
            ],
          ),
),
    ]
    );
  }
}



