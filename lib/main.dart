import 'package:farmgate/page/bidOrder.dart';
import 'package:farmgate/page/bookmark.dart';
import 'package:farmgate/page/changePassword.dart';
import 'package:farmgate/page/createBid.dart';
import 'package:farmgate/page/editProfile.dart';
import 'package:farmgate/page/filterPage.dart';
import 'package:farmgate/page/login.dart';
import 'package:farmgate/page/productDetails.dart';
import 'package:farmgate/page/register.dart';
import 'package:farmgate/page/welcomePage.dart';
import 'package:farmgate/splashscreen.dart';
import 'package:farmgate/widgets/bottomNav.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//Started on 25th September 2020
//By Makinde Daniel Ayomide

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Farm Gate',
        debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: GoogleFonts.muliTextTheme(
          Theme.of(context).textTheme,
        ),
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:AnimatedSplashScreen(),
      routes: <String,WidgetBuilder>{
         "/WelcomePage": (BuildContext context) => new WelcomePage(),
         "/Register": (BuildContext context) => new Register(),
         "/BottomNav": (BuildContext context) => new BottomNav(),
         "/Login": (BuildContext context) => new Login(),
        "/BookMark": (BuildContext context) => new BookMark(),
        "/ProductDetails": (BuildContext context) => new ProductDetails(),
        "/CreateBid": (BuildContext context) => new CreateBid(),
        "/BidOrder": (BuildContext context) => new BidOrder(),
        "/EditProfile": (BuildContext context) => new EditProfile(),
        "/ChangePassword": (BuildContext context) => new ChangePassword(),
        "/Filter": (BuildContext context) => new Filter(),








      }
    );
  }
}
