import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/page/bid.dart';
import 'package:farmgate/page/home.dart';
import 'package:farmgate/page/login.dart';
import 'package:farmgate/page/notification.dart';
import 'package:farmgate/page/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class BottomNav extends StatefulWidget {
 final int bottomNo;

  BottomNav({Key key,this.bottomNo}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState(bottomNo);
}
//ListWheelScrollView

class _MyStatefulWidgetState extends State<BottomNav> {
  int bottomNo;

  _MyStatefulWidgetState(this.bottomNo);
  List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
      icon:
      ImageIcon(
        AssetImage('images/home.png',),
      ),
      label: 'Home',
    ),
    BottomNavigationBarItem(
      icon:
      ImageIcon(
        AssetImage('images/bids.png'),
      ),
      label: 'Bid',
    ),
    BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage('images/notif.png'),
      ),
      label: 'Notification',
    ),
    BottomNavigationBarItem(
      icon: ImageIcon(
        AssetImage('images/profile.png'),
      ),
      label: 'Profile',
    ),
  ];

  int _selectedIndex = 0;

  List<Widget> pageList = <Widget>[

    Home(),
    Bid(),
    Notifications(),
    Profile(),
  ];

@override
  void initState() {
    // TODO: implement initState
 // badge();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {



    return Scaffold(
//backgroundColor: AppColor.deepGreen,
      body:

      pageList[_selectedIndex],

      bottomNavigationBar:
     BottomNavigationBar(
       type: BottomNavigationBarType.fixed,
        onTap:(newIndex) => setState(() => _selectedIndex = newIndex),
        currentIndex: _selectedIndex,
        items:items,
        selectedItemColor: AppColor.deepGreen,
      ),

    );
  }
}