import 'package:farmgate/helper/theme.dart';
import 'package:flutter/material.dart';

class GeneralButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final BorderRadius borderRadius;
  final Color splashColor;
  final String buttonText;
  final Color buttonTextColor;
  GeneralButton({Key key, this.child, this.onPressed, this.borderRadius = const BorderRadius.all(Radius.circular(5)), this.splashColor = appTheme, this.buttonText, this.buttonTextColor = const Color(0xffFFFFFF)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    return
      Container(
          //padding: EdgeInsets.all(20),
          height: 44,
          width: deviceWidth,
          child:
          RaisedButton(

            shape: RoundedRectangleBorder(
              borderRadius: borderRadius,
                side: BorderSide(color:AppColor.deepGreen)

            ),
            color: splashColor,
            onPressed: () {
              if (onPressed != null) {
                      onPressed();
                    }
                  },
            child: Text(buttonText, style: TextStyle(color: buttonTextColor, fontWeight: FontWeight.w600),),

          )
      );

  }
}
