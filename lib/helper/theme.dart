import 'package:flutter/material.dart';

const  appTheme = const Color(0xff034952);
class AppColor {
  static final Color deepGreen = appTheme;
  static final Color grey = Color(0xffC4C4C4);
  static final Color goldGrey = Color(0xff8B8B8B);
  static final Color white = Color(0xffFFFFFF);
  static final Color black = Color.fromRGBO(0, 0, 0, 1.0);
  static final Color lemonGreen = Color(0xdd41B66D);
  static final Color green = Color(0xff41B66D);
  static final Color gold = Color(0xff937C2A);
  static final Color pendingRed = Color(0xffD18134);
  static final Color notApprovedRed = Color(0xff8F1838);
  static final Color paleSky = Color.fromRGBO(101, 119, 133, 1.0);
  static final Color ceriseRed = Color.fromRGBO(224, 36, 94, 1.0);
  static final Color paleSky50 = Color.fromRGBO(101, 118, 133, 0.5);
}
