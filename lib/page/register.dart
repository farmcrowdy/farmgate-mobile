import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _Register createState() => _Register();
}

class _Register extends State<Register> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  bool passwordVisible;
  String bank;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child:
          Container(
        padding: EdgeInsets.all(10),
            child:
          Column(
            children: <Widget>[
              SizedBox(height: 20,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [

                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child:
                      Icon(
                          Icons.arrow_back_ios
                      ),
                      ),
                    ],
                  ),
               SizedBox(height: 40,),

                  Text(
                      'Sign up',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),

                  SizedBox(height: 10,),

                  Text(
                    'Get started by filling the form',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,),
                  ),
                  Text(
                    'below with the required information needed.',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,),
                  ),

SizedBox(height: 20,),
                        TextFormField(
                          controller: numberController,
                          // maxLength: 11,
                          decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                            hintText: 'First Name',
                            hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                            ),
                            labelStyle: TextStyle(color: Colors.blue),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.black,
                        ),
                  SizedBox(height: 20,),
                  TextFormField(
                    controller: numberController,
                    // maxLength: 11,
                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                      hintText: 'Last Name',
                      hintStyle: TextStyle(
                          color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                      ),
                      labelStyle: TextStyle(color: Colors.blue),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black),
                    cursorColor: Colors.black,
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    controller: numberController,
                    // maxLength: 11,
                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                      hintText: 'Email',
                      hintStyle: TextStyle(
                          color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                      ),
                      labelStyle: TextStyle(color: Colors.blue),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black),
                    cursorColor: Colors.black,
                  ),
                  SizedBox(height: 20,),
                  TextFormField(
                    controller: numberController,
                    // maxLength: 11,
                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                      hintText: 'Phone Number',
                      hintStyle: TextStyle(
                          color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                      ),
                      labelStyle: TextStyle(color: Colors.blue),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black),
                    cursorColor: Colors.black,
                  ),
                  SizedBox(height: 20,),
                  Container(
                      padding: EdgeInsets.only(left: 10),
                      height: 60.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          border: Border.all(color: Colors.grey)),
                      child:
                      Center(
                        child: DropdownButtonFormField<String>(

                          isExpanded: true,
                          value: bank,

                          onChanged: (String newValue) {
                            setState(() {
                              bank = newValue;
                            });
                          },
                          decoration: InputDecoration.collapsed(
                              hintText: 'Bank Name', hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),   ),
                          items: <String>[
                            'GTbank',
                            'First Bank',
                            'UBA',
                          ].map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                        ),
                      )
                  ),

                  SizedBox(height: 20,),
                  TextFormField(
                    controller: numberController,
                    // maxLength: 11,
                    decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                      hintText: 'Account Number',
                      hintStyle: TextStyle(
                          color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                      ),
                      labelStyle: TextStyle(color: Colors.blue),
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black),
                    cursorColor: Colors.black,
                  ),
SizedBox(height: 20,),
                        TextFormField(
                          controller: passwordController,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                            hintText: 'Password',
                            hintStyle: TextStyle(
                              color: Colors.black,fontWeight: FontWeight.bold
                            ),
                            labelStyle: TextStyle(color: Colors.blue),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          obscureText: passwordVisible,
                          keyboardType: TextInputType.visiblePassword,
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.black,
                        ),

                  SizedBox(height: 20,),
                  GeneralButton(
                    buttonText: 'Join Farmgate',
                    buttonTextColor: AppColor.white,
                  ),

SizedBox(height: 20,),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Have an account? ",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.normal),
                        ),
                        new GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, "/Login");
                          },
                          child: new Text(
                            "Log in here",
                            style: TextStyle(
                              color: AppColor.deepGreen,
                              fontWeight: FontWeight.w600,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          )),
      )
    );
  }
}
