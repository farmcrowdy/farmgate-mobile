import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:farmgate/helper/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class Profile extends StatefulWidget {
  @override
  _Pickup createState() => _Pickup();
}

class _Pickup extends State<Profile> with SingleTickerProviderStateMixin {



  @override
  Widget build(BuildContext context) {

    final deviceWidth = MediaQuery.of(context).size.width;
    return
      Scaffold(

          body:
          SingleChildScrollView(
            child:

            Column(

              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                Container(
                  padding: EdgeInsets.only(top:60),
                  child:
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
Container(
                  padding: EdgeInsets.all(10),
  child:
                          Text(
                             'Profile',
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20 ),
                                ),
                ),
                        ]
                      ),


                      Center(
                        child:

                        CircleAvatar(
                          radius: 70,
                          backgroundColor:
                          AppColor.gold.withOpacity(0.2),
                          child:  Image(
                            image: AssetImage('images/placeHolder.png'),
                          ),
                        ),
                      ),
SizedBox(
  height: 100,
),
  Divider(thickness: 1,),
InkWell(
  onTap: () => Navigator.pushNamed(context, "/EditProfile"),
  child:
Container(
  padding: EdgeInsets.all(20),
  child: 
                      Row(
                        children: <Widget>[
                          Container(
                            height: 36,
                            width: 36,
                            child:
                        Image(
                              image:
                          AssetImage('images/edit.png'),),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Edit Details', style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold
                              ),
                              ),
                              SizedBox(
                                height: 5,
                              ),

                            ],
                          )


                        ],
                      )
),
),
                      Divider(thickness: 1,),
                      InkWell(
                        onTap: () => Navigator.pushNamed(context, "/ChangePassword"),
                        child:

                      Container(
                          padding: EdgeInsets.all(20),
                          child:
                          Row(
                            children: <Widget>[
                              Container(
                                height: 36,
                                width: 36,
                                child:
                                Image(

                                  image:
                                  AssetImage('images/changePassword.png'),),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  Text(
                                    'Change Password', style: TextStyle(
                                      fontSize: 14, fontWeight: FontWeight.bold
                                  ),
                                  ),

                                ],
                              )
                            ],
                          ),
                      ),
                      ),
                      Divider(thickness: 1,),
SizedBox(height: 20,),
Center(
  child:
      InkWell(
        onTap: (){

        },
        child:
                      Container(
                        height: 36,
                        width: 36,
                        child:
                        Image(

                          image:
                          AssetImage('images/logOut.png'),),
                      ),
      ),
),
            ],
          ),

          ),
        ]
      ),
          ),

      );
  }
}
