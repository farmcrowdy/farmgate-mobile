import 'package:farmgate/helper/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';

class Bid extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Bid> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  String title = 'Order';
  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Bids - $title',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      ),
                      Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              height: 24,
                              width: 24,
                              child: Image(
                                image: AssetImage('images/filter.png'),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/BookMark');
                              },
                              child: Container(
                                  height: 24,
                                  width: 24,
                                  child: Image(
                                    image: AssetImage('images/bookMark.png'),
                                  ))),
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ToggleSwitch(
                        cornerRadius: 3,
                        minWidth: 150.0,
                        initialLabelIndex: 0,
                        activeBgColor:  AppColor.deepGreen,
                        activeFgColor: Colors.white,
                        inactiveBgColor: Colors.white,
                        inactiveFgColor: Color(0xff9F9F9C),
                        labels: ['Order', 'Collection'],
                        onToggle: (index) {
                          print('switched to: $index');
                          if (index == 0) {

                            setState(() {
                              title = 'Order';
                            });
                          } else if (index == 1) {
                          //  id = '1';
                            setState(() {
                              title = 'Collection';
                              //toggleId = _fetch3();
                            });


                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.pushNamed(context, '/BidOrder');
                    },
                    child:
                  Card(
                    elevation: 1,

                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/cutMaize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Bid Awaiting Approval',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              width: 80,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Pending',
                                                    style: TextStyle(
                                                        color: AppColor.pendingRed, fontSize: 12),
                                                  ),
                                                  color: AppColor.pendingRed
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Text('')
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/cutMaize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Bid Approved - Response Required',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 8,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              width: 80,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Active',
                                                    style: TextStyle(
                                                        color: AppColor.green, fontSize: 12),
                                                  ),
                                                  color: AppColor.green
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Text('')
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/cutMaize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Bid Not Approved',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              width: 80,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Failed',
                                                    style: TextStyle(
                                                        color: AppColor.notApprovedRed, fontSize: 12),
                                                  ),
                                                  color: AppColor.notApprovedRed
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Text('')
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                ],
              ),
            ],
          )),
    ));
  }
}
