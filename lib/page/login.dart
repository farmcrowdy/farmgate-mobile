import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  bool passwordVisible;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child:
          Container(
        padding: EdgeInsets.all(10),
            child:
          Column(
            children: <Widget>[
              SizedBox(height: 20,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: [

                      InkWell(
                        onTap: (){
                          Navigator.pop(context);
                        },
                        child:
                      Icon(
                          Icons.arrow_back_ios
                      ),
                      ),
                    ],
                  ),
               SizedBox(height: 40,),

                  Text(
                      'Welcome',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),

                  SizedBox(height: 10,),

                  Text(
                    'Login with either your',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,),
                  ),
                  Text(
                    'email or phone number to get in.',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,),
                  ),

SizedBox(height: 30,),
                        TextFormField(
                          controller: numberController,
                          // maxLength: 11,
                          decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                            hintText: 'Email or Phone Number',
                            hintStyle: TextStyle(
                              color: Colors.black45,fontSize: 14
                            ),
                            labelStyle: TextStyle(color: Colors.blue),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.black,
                        ),
SizedBox(height: 30,),
                        TextFormField(
                          controller: passwordController,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Colors.black,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            ),
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                            hintText: 'Password',
                            hintStyle: TextStyle(
                              color: Colors.black45,
                            ),
                            labelStyle: TextStyle(color: Colors.blue),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          obscureText: passwordVisible,
                          keyboardType: TextInputType.visiblePassword,
                          style: TextStyle(color: Colors.black),
                          cursorColor: Colors.black,
                        ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(

                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[

                        new GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, "/ResetPassword");
                          },
                          child: new Text(
                            "Forgot password?",
                            style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w600,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30,),
                  GeneralButton(
                    buttonText: 'Log in',
                    buttonTextColor: AppColor.white,
                    onPressed: (){
                      Navigator.pushNamed(context, '/BottomNav');
                    },
                  ),

SizedBox(height: 20,),
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Have no account? ",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.normal),
                        ),
                        new GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, "/Register");
                          },
                          child: new Text(
                            "Sign up here",
                            style: TextStyle(
                              color: AppColor.deepGreen,
                              fontWeight: FontWeight.w600,
                              fontSize: 13,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          )),
      )
    );
  }
}
