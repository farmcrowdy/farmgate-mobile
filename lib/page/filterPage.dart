import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Filter extends StatefulWidget {
  @override
  _Register createState() => _Register();
}

class _Register extends State<Filter> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child: Container(
        child: Column(children: <Widget>[
          SizedBox(
            height: 40,
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 50,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Color(0xccDFDED2),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 2,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ]),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        onTap:(){
                          Navigator.pop(context);
          },
                        child:  Text('Close'),
                      ),

                      Text('Filter'),
                      Text('Done'),
                    ],
                  ),
                ),
                Divider(
                  thickness: 2,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    'Harvest',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          height: 85,
                          width: 122,
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(10),
                                height: 50,
                                width: 50,
                                child: Image(
                                  image: AssetImage('images/blackMaize.png'),
                                ),
                              ),
                              Text(
                                'Maize',
                                style: TextStyle(fontSize: 10),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    'Status',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Active',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Inactive',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 0),
                  child: Text(
                    'Remaining Delivery Timeline',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '1 day to go',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '2 days to go',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa034952).withOpacity(0.1),
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(color: Colors.grey)),
                            height: 44,
                            width: 160,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '3 days to go',
                                  style: TextStyle(
                                      fontSize: 12, color: Color(0xff034952)),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa034952).withOpacity(0.1),
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(color: Colors.grey)),
                            height: 44,
                            width: 160,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '4 days & more to go',
                                  style: TextStyle(
                                      fontSize: 12, color: Color(0xff034952)),
                                )
                              ],
                            )),
                      ]),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa034952).withOpacity(0.1),
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(color: Colors.grey)),
                            height: 44,
                            width: 160,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '3 days to go',
                                  style: TextStyle(
                                      fontSize: 12, color: Color(0xff034952)),
                                )
                              ],
                            )),
                        Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xaa034952).withOpacity(0.1),
                                borderRadius: BorderRadius.circular(3.0),
                                border: Border.all(color: Colors.grey)),
                            height: 44,
                            width: 160,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '4 days & more to go',
                                  style: TextStyle(
                                      fontSize: 12, color: Color(0xff034952)),
                                )
                              ],
                            )),
                      ]),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    'Quantity Amount',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 99,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '10,000+',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 99,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '20,000+',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 99,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '30,000+',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Text(
                    'Quantity Unit',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Metric Tonnes(MT)',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                      Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xaa034952).withOpacity(0.1),
                              borderRadius: BorderRadius.circular(3.0),
                              border: Border.all(color: Colors.grey)),
                          height: 44,
                          width: 160,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Litre (L)',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xff034952)),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                Divider(),
                Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Reset Filter',
                          style: TextStyle(
                              color: Color(0xff034952),
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          '42 Results',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    )),
              ]),
        ]),
      )),
    );
  }
}
