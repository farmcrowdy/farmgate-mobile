import 'dart:convert';
import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class CreateBid extends StatefulWidget {
  @override
  _ProductDetails createState() => _ProductDetails();
}

class _ProductDetails extends State<CreateBid> {
  String bank;
  String lg;
  String state;
  var dateBirth;
  var dateString;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;
    TextEditingController numberController = TextEditingController();




    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Stack(
            // alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                child: Image.asset(
                  'images/largeMaize.png',
                  width: deviceWidth,
                  height: 156,
                  fit: BoxFit.fill,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 100,
                  width: 80,
                  //margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Image.asset(
                    'images/back.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
          Container(
              padding: EdgeInsets.all(10),
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Text(
                      'Maize',
                      style: TextStyle(
                          color: AppColor.black,
                          fontSize: 36,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 25,
                        width: 65,
                        child: FlatButton(
                            child: Text(
                              'Active',
                              style: TextStyle(
                                  color: AppColor.green, fontSize: 10),
                            ),
                            color: AppColor.green.withOpacity(0.2),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            onPressed: () {
                              //   Navigator.of(context).pushNamed('/signup');
                            })),
                  ]),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    '5% commission',
                    style: TextStyle(
                      color: AppColor.goldGrey,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 16,
                            child: Image(
                              image: AssetImage('images/bag.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            '3,000MT',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 16,
                            child: Image(
                              image: AssetImage('images/clock.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            '3 days left',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 11,
                            child: Image(
                              image: AssetImage('images/location.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Lagos State',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.only(left: 10),
            color: AppColor.gold.withOpacity(0.2),
            height: 42,
            width: deviceWidth,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Fill the below form to create your bid',
                    style: TextStyle(
                        color: AppColor.gold,
                        fontWeight: FontWeight.w600,
                        fontSize: 12),
                  )
                ]),
          ),

          Container(
            padding: EdgeInsets.all(10),
            child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: [
    Container(
      width:170,
      child:
    TextFormField(

      controller: numberController,
      // maxLength: 11,
      decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
        hintText: 'Total Quantity to be Delivered',
        hintStyle: TextStyle(
          color: Colors.black45,fontSize: 10,
        ),
        labelStyle: TextStyle(color: Colors.blue),
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(5.0),
          borderSide: new BorderSide(),
        ),
      ),
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(color: Colors.black),
      cursorColor: Colors.black,
    ),
    ),
SizedBox(width: 10,),
    Container(
        padding: EdgeInsets.only(left: 10),
        height: 60.0,
        width: 170,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7.0),
            border: Border.all(color: Colors.grey)),
        child:
        Center(
          child: DropdownButtonFormField<String>(

            isExpanded: true,
            value: bank,

            onChanged: (String newValue) {
              setState(() {
                bank = newValue;
              });
            },
            decoration: InputDecoration.collapsed(
              hintText: 'Unit of Measurement', hintStyle: TextStyle(
              color: Colors.black45,fontSize: 14,
            ),   ),
            items: <String>[
              'Metric Tonnes',
              'Kg',
            ].map<DropdownMenuItem<String>>(
                    (String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
          ),
        )
    ),

  ],
),


                SizedBox(height: 20,),

                TextFormField(
                  controller: numberController,
                  // maxLength: 11,
                  decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                    hintText: 'Price per Quantity',
                    hintStyle: TextStyle(
                      color: Colors.black45,fontSize: 14,
                    ),
                    labelStyle: TextStyle(color: Colors.blue),
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      borderSide: new BorderSide(),
                    ),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(color: Colors.black),
                  cursorColor: Colors.black,
                ),
                SizedBox(height: 20,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                Container(
                    padding: EdgeInsets.only(left: 10),
                    height: 60.0,
                    width: 180,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        border: Border.all(color: Colors.grey)),
                    child:
                    Center(
                      child: DropdownButtonFormField<String>(

                        isExpanded: true,
                        value: state,

                        onChanged: (String newValue) {
                          setState(() {
                            state = newValue;
                          });
                        },
                        decoration: InputDecoration.collapsed(
                          hintText: 'State Location of Harvest', hintStyle: TextStyle(
                          color: Colors.black45,fontSize: 12,
                        ),   ),
                        items: <String>[
                          'Lagos',
                          'Ibadan',
                        ].map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                      ),
                    )
                ),

                //SizedBox(height: 20,),

                Container(
                    padding: EdgeInsets.only(left: 10),
                    height: 60.0,
                    width: 180,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        border: Border.all(color: Colors.grey)),
                    child:
                    Center(
                      child: DropdownButtonFormField<String>(

                        isExpanded: true,
                        value: lg,

                        onChanged: (String newValue) {
                          setState(() {
                            lg = newValue;
                          });
                        },
                        decoration: InputDecoration.collapsed(
                          hintText: 'LGA Location of Harvest', hintStyle: TextStyle(
                          color: Colors.black45,fontSize: 12,
                        ),   ),
                        items: <String>[
                          'Island',
                          'Isolo',
                        ].map<DropdownMenuItem<String>>(
                                (String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                      ),
                    )
                ),
                ]
              ),
                SizedBox(height: 20,),
                InkWell(
                  onTap: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true, onChanged: (date) {
                          print('change $date in time zone ' +
                              date.toUtc().toString());
                        }, onConfirm: (date) {
                          setState(() {
                            dateBirth = date;
                            //06-Jun-96
                            //dateString = DateFormat('dd-MMM-yy').format(date);
                            dateString = DateFormat('dd/MM/y').format(date);
                            // age = DateFormat.yMd().format(date);
                            // time = date.toLocal();
                            print('confirm $dateBirth');
                          });
                        }, currentTime: DateTime.now());
                  },
                  child: Container(
                      width: deviceWidth,
                      padding: EdgeInsets.all(10),
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          border: Border.all(color: Colors.grey)),
                      child: Center(
                        child: FlatButton(
                            onPressed: () {
                              DatePicker.showDatePicker(context,
                                  minTime: DateTime.now(),
                                  showTitleActions: true, onChanged: (date) {
                                    print('change $date in time zone ' +
                                        date.toUtc().toString());
                                  }, onConfirm: (date) {
                                    setState(() {
                                      dateBirth = date;
                                      //06-Jun-96
                                      //dateString = DateFormat('dd-MMM-yy').format(date);
                                      dateString = DateFormat('dd/MM/y').format(date);
                                      // age = DateFormat.yMd().format(date);
                                      // time = date.toLocal();
                                      print('confirm $dateBirth');
                                    });
                                  }, currentTime: DateTime.now());
                            },
                            child:
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        dateString != null
            ?
        //time.toString() + ' / ' + dater.toString():
        dateString.toString()
            : 'Select Delivery Date',
        style: TextStyle(
            color: Colors.black,
            fontSize: 15,
            fontWeight: FontWeight.w500),
      ),
      Container(
        height: 16,
        width: 17,
        child: Image(
          image: AssetImage('images/calender.png'),
        ),
      ),

    ],
    )

                            ),
                      )),
                ),

                SizedBox(height: 20,),

          SizedBox(height: 20,),
          GeneralButton(
            buttonText: 'Create Bid',
            buttonTextColor: AppColor.white,
          ),
        ],
      )),
        ]
          ),
      )
    );
  }
}
