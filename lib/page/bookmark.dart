import 'package:farmgate/helper/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookMark extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<BookMark> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                      Row(
                        children: [
                          InkWell(
                            onTap: (){
                              Navigator.pop(context);
                            },
                            child:
                            Icon(
                                Icons.arrow_back_ios
                            ),
                          ),
                          Text(
                            'Bookmark',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),

                      Row(
                        children: [
                          InkWell(
                            onTap: () {},
                            child: Container(
                              height: 24,
                              width: 24,
                              child: Image(
                                image: AssetImage('images/filter.png'),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          InkWell(
                              onTap: () {},
                              child: Container(
                                  height: 24,
                                  width: 24,
                                  child: Image(
                                    image: AssetImage('images/bookMarkC.png'),
                                  ))),
                        ],
                      )
                    ],
                  ),

                  SizedBox(
                    height: 20,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/maize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Farmcrowdy',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Active',
                                                    style: TextStyle(
                                                        color: AppColor.green),
                                                  ),
                                                  color: AppColor.green
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Image(
                                    image: AssetImage('images/bookMarkC.png'),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/maize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Farmcrowdy',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Active',
                                                    style: TextStyle(
                                                        color: AppColor.green),
                                                  ),
                                                  color: AppColor.green
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Image(
                                    image: AssetImage('images/bookMarkC.png'),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/maize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Farmcrowdy',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Active',
                                                    style: TextStyle(
                                                        color: AppColor.green),
                                                  ),
                                                  color: AppColor.green
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Image(
                                    image: AssetImage('images/bookMarkC.png'),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    elevation: 1,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 105,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 52,
                                    width: 52,
                                    child: Image(
                                      image: AssetImage('images/maize.png'),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(children: [
                                          Text(
                                            'Farmcrowdy',
                                            style: TextStyle(
                                              color: AppColor.goldGrey,
                                              fontSize: 10,
                                            ),
                                          ),
                                          Container(
                                              margin: EdgeInsets.only(left: 10),
                                              height: 25,
                                              child: FlatButton(
                                                  child: Text(
                                                    'Active',
                                                    style: TextStyle(
                                                        color: AppColor.green),
                                                  ),
                                                  color: AppColor.green
                                                      .withOpacity(0.2),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(18.0),
                                                    //side: BorderSide(color: Colors.red)
                                                  ),
                                                  onPressed: () {
                                                    //   Navigator.of(context).pushNamed('/signup');
                                                  })),
                                        ]),
                                        Text(
                                          'Maize',
                                          style: TextStyle(
                                              color: AppColor.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/bag.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3,000MT',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  height: 14,
                                                  width: 14,
                                                  child: Image(
                                                    image: AssetImage(
                                                        'images/clock.png'),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  '3 days left',
                                                  style: TextStyle(
                                                      color: AppColor.black,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ]),
                                ]),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  height: 18,
                                  width: 14,
                                  child: Image(
                                    image: AssetImage('images/bookMarkC.png'),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(
                                  '3hrs',
                                  style: TextStyle(
                                      color: AppColor.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )
                          ],
                        )),
                  )
                ],
              ),
            ],
          )),
    ));
  }
}
