import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

  class EditProfile extends StatefulWidget {
  @override
  _Register createState() => _Register();
}

class _Register extends State<EditProfile> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController numberController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  bool passwordVisible;
  String bank;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        key: _scaffoldKey,
        body: SingleChildScrollView(
          child:
          Container(
              padding: EdgeInsets.all(10),
              child:
              Column(
                children: <Widget>[
                  SizedBox(height: 20,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [

                          InkWell(
                            onTap: (){
                              Navigator.pop(context);
                            },
                            child:
                            Icon(
                                Icons.arrow_back_ios
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 40,),

                      Text(
                        'Edit Profile',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.w600),
                      ),

                      SizedBox(height: 20,),
                      TextFormField(
                        controller: numberController,
                        // maxLength: 11,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'First Name',
                          hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                      SizedBox(height: 20,),
                      TextFormField(
                        controller: numberController,
                        // maxLength: 11,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Last Name',
                          hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                      SizedBox(height: 20,),
                      TextFormField(
                        controller: numberController,
                        // maxLength: 11,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Email',
                          hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                      SizedBox(height: 20,),
                      TextFormField(
                        controller: numberController,
                        // maxLength: 11,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Phone Number',
                          hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),
                      SizedBox(height: 20,),
                      Container(
                          padding: EdgeInsets.only(left: 10),
                          height: 60.0,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              border: Border.all(color: Colors.grey)),
                          child:
                          Center(
                            child: DropdownButtonFormField<String>(

                              isExpanded: true,
                              value: bank,

                              onChanged: (String newValue) {
                                setState(() {
                                  bank = newValue;
                                });
                              },
                              decoration: InputDecoration.collapsed(
                                hintText: 'Bank Name', hintStyle: TextStyle(
                                  color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                              ),   ),
                              items: <String>[
                                'GTbank',
                                'First Bank',
                                'UBA',
                              ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                            ),
                          )
                      ),

                      SizedBox(height: 20,),
                      TextFormField(
                        controller: numberController,
                        // maxLength: 11,
                        decoration: InputDecoration(
//                    errorText: residentialValid
//                        ? null
//                        : 'Kindly update this field',
                          hintText: 'Account Number',
                          hintStyle: TextStyle(
                              color: Colors.black,fontSize: 14, fontWeight: FontWeight.bold
                          ),
                          labelStyle: TextStyle(color: Colors.blue),
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(5.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.black),
                        cursorColor: Colors.black,
                      ),

                      SizedBox(height: 20,),
                      GeneralButton(
                        buttonText: 'Save Details',
                        buttonTextColor: AppColor.white,
                      ),

                    ],
                  ),
                ],
              )),
        )
    );
  }
}
