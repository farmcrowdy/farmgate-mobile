import 'dart:convert';
import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BidOrder extends StatefulWidget {
  @override
  _ProductDetails createState() => _ProductDetails();
}

class _ProductDetails extends State<BidOrder> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Stack(
            // alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                child: Image.asset(
                  'images/largeMaize.png',
                  width: deviceWidth,
                  height: 156,
                  fit: BoxFit.fill,
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  height: 100,
                  width: 80,
                  //margin: EdgeInsets.only(top: 20),
                  padding: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Image.asset(
                    'images/back.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
          Container(
              padding: EdgeInsets.all(10),
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Text(
                      'Maize',
                      style: TextStyle(
                          color: AppColor.black,
                          fontSize: 36,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 25,
                        width: 65,
                        child: FlatButton(
                            child: Text(
                              'Active',
                              style: TextStyle(
                                  color: AppColor.green, fontSize: 10),
                            ),
                            color: AppColor.green.withOpacity(0.2),
                            shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              //side: BorderSide(color: Colors.red)
                            ),
                            onPressed: () {
                              //   Navigator.of(context).pushNamed('/signup');
                            })),
                  ]),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Bid Approved - Response Required',
                    style: TextStyle(
                      color: AppColor.goldGrey,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 16,
                            child: Image(
                              image: AssetImage('images/bag.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            '3,000MT',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 16,
                            child: Image(
                              image: AssetImage('images/clock.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            '3 days left',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            height: 16,
                            width: 11,
                            child: Image(
                              image: AssetImage('images/location.png'),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Lagos State',
                            style: TextStyle(
                                color: AppColor.black,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              )),
          Container(
            padding: EdgeInsets.only(left: 10),
            color: AppColor.gold.withOpacity(0.2),
            height: 42,
            width: deviceWidth,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Bid Detail',
                    style: TextStyle(
                        color: AppColor.gold,
                        fontWeight: FontWeight.w600,
                        fontSize: 12),
                  )
                ]),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total Quantity:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '2,000',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Unit:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Metric Tonnes',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Price per Quantity:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '500',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Price Currency:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Naira',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Delivery Location:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Ota, Ogun State',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),

          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Delivery Date:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Sept. 30 2020',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total Amount:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'N1,000,000',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '5% Commission:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'N50,000',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
              height: 120,
              width: deviceWidth,
              child: Card(
                  elevation: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(20),
                        child: Text('Would you be able to deliver?'),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  width: 69,
                                  child: GeneralButton(
                                    buttonText: 'NO',
                                    buttonTextColor: AppColor.deepGreen,
                                    splashColor: AppColor.white,
                                  )),
                              SizedBox(
                                width: 30,
                              ),
                              Container(
                                  width: 250,
                                  padding: EdgeInsets.only(right: 20),
                                  child: GeneralButton(
                                    onPressed: (){
                                    },
                                    buttonText: 'Yes, I would',
                                  )),
                            ],
                          )),
                    ],
                  ))),
          Container(
            padding: EdgeInsets.only(left: 10),
            color: AppColor.gold.withOpacity(0.2),
            height: 42,
            width: deviceWidth,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Basic Requirement',
                    style: TextStyle(
                        color: AppColor.gold,
                        fontWeight: FontWeight.w600,
                        fontSize: 12),
                  )
                ]),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Size:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Whole Grain',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Color:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'White',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Hardness:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Soft',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Test weight:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Bushel',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Drying process:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  'Air Dried',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),

          Container(
            padding: EdgeInsets.only(left: 10),
            color: AppColor.gold.withOpacity(0.2),
            height: 42,
            width: deviceWidth,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Percentage Requirement',
                    style: TextStyle(
                        color: AppColor.gold,
                        fontWeight: FontWeight.w600,
                        fontSize: 12),
                  )
                ]),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Splits:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '18.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Weevil:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Volatile:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '2.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Moisture:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '2.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),

          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total Defects:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '1.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Foreign matter:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Broken Matter:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Insect Defiled %:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.5%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Damage Kernel:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '1.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Rotten/Shrivelled:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '1.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Hectolites Weight:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '4.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Acid Insoluble Ash:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '2.0%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Curcumin Content:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.5%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Mold % by weight:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.5%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Whole Dead Insects:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.5%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Extraneous % by weight:',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  '0.5%',
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: AppColor.goldGrey),
                )
              ],
            ),
          ),
        ],
      )),
    );
  }
}
