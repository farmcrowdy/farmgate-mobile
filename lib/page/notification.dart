import 'package:farmgate/helper/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Notifications> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(20),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      Text(
                        'Notifications',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Approved',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Delivered',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Approved',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Delivered',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Approved',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Approved',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                      Container(
                        height: 100,
                        // width: deviceWidth,
                        child: Column(children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        AppColor.gold.withOpacity(0.2),
                                    child: Text(
                                      'FC',
                                      style: TextStyle(
                                        color: AppColor.gold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Text(
                                    'Maize Bid Approved',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w600),
                                  )
                                ],
                              ),
                              Text(
                                '2h',
                                style: TextStyle(
                                    fontSize: 12, color: AppColor.goldGrey),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Maize bid approved, your response is required to determine if delivery is still on course.',
                            style: TextStyle(
                                fontSize: 12, color: AppColor.goldGrey),
                          )
                        ]),
                      ),
                    ]))));
  }
}
