import 'package:farmgate/helper/theme.dart';
import 'package:farmgate/widgets/generalButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<WelcomePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final deviceWidth = MediaQuery.of(context).size.width;

    return Stack(// <-- STACK AS THE SCAFFOLD PARENT
        children: [
      Container(
        color: Colors.white,
        width: deviceWidth,
        child: Image.asset(
          'images/background.png',
          fit: BoxFit.fill,
        ),
      ),
      Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child:
    Container(
    padding: EdgeInsets.all(20),
    child:
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  //padding: EdgeInsets.all(50),
                  height: 75,
                  width: 258,
                  child: new Image.asset(
                    'images/largeLogo.png',
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Welcome to Farmgate',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),

                Text(
                  'An application to gather information on grain points',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                     ),
                ),
                SizedBox(height: 80,),
                GeneralButton(
                  buttonText: 'Sign up',
                  onPressed: () {
                    Navigator.pushNamed(context, "/Register");
                  },
                ),
                SizedBox(height: 20,),
                GeneralButton(
                  buttonText: 'Log in',
                  buttonTextColor: AppColor.deepGreen,
                  splashColor: Colors.white.withOpacity(0.9),
                  onPressed: () {
Navigator.pushNamed(context, '/Login');
                  },
                ),
              ],
            ),
          ))
      )
    ]);
  }
}
